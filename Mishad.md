# GitLab CI/CD - Part 1


### DEPLOY GITLAB RUNNER AS A DOCKER CONTAINER

First you need to create a volume in docker to install GitLab Runner as a container.
```bash
docker volume create gitlab-runner-config
```

Then, you need to start the GitLab-Runner by mounting the docker volume created earlier in the instance which you are going to install GitLab Runner.
```bash
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

Now, You can check GitLab-Runner docker container in your instance using,
```bash
docker ps
```


### REGISTER THE GITLAB RUNNER

After that, You need to register that runners with GitLab. For Docker volume mount gitlab runner excute the following coommand to begin registrstion process,
```bash
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register
```

In the registration process you need to provide,

- GitLab instance URL - URL is avilable in;
`settings --> cicd --> runners --> registration tokn`

- Registration token - Token is also avilable in;
`settings --> cicd --> runners --> registration token`

- Description for the runner - Give a name for the runner as you wish
- You can skip tag & maintenance note section using enter key
- Set the Executor as `docker`
- Set the default Docker image as `docker:20.10.16`

Then, refresh the GitLab instance and go to `settings --> cicd --> runners` section ; you can see that set runner have avilable in under the `Available specific runners` section with green dot mark.

### DOCKER SOCKET BINDING

If you are using images which is not support docker in docker service (dind) inside that image container in the cicd pipeline process, You will be needed to do Docker Socket Binding to allocate docker in docker service inside that images container. For that;

Get the gitlab-runner container ID using;
```bash
docker ps
```

Log into gitlab-runner container using that ID.
```bash
docker exec -it <container_id> /bin/bash
```

Update the gitlab-runner container os and install the vim package using following commands.
```bash
apt-get update
apt install vim
```



Excute following command to open configuration file related to gitlab-runner.
```bash
vim /etc/gitlab-runner/config.toml
```

Edit the volume section as below which is in under runners main section under to your gitlab-runner register description name.
```bash
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```

Save the file and exit from gitlab-runner container using `exit` command.

After that, once you do a change in container make sure to restart the container. You might need to restart the runner to apply the changes. Make sure to restart the whole container instead of using gitlab-runner restart.
```bash
docker restart gitlab-runner
```

### .gitlab-ci.yml FILE CONFIGURATIONS

Now, Move on to the `.gitlab-ci.yml` file script. This may be changed according to your project. In my example project, I have planed to divide this script into four stages. Such as test, build, push and deploy.

### TEST STAGE

In the test stage I have used openjdk:11 image to run unit tests. In this image docker container can't allocate dind (docker-in-docker) service. Then, that docker socker binding helps to access Docker via a TCP socket in openjdk:11 image container. I keep the `.gradle/wrapper`, `.gradle/caches` files in the cache for next stages. Otherwise, we have to download that files again and again in the next stages. You can cache your files as your wish. I have named that cache as `gradle-cache-key`. 

Following script section is related to test stage in gitlab CI/CD pipeline.

```bash
test:
  stage: test
  image: openjdk:11
  only:
    - development
  before_script:
  - echo `pwd` # debug
#  - echo "$CI_BUILD_NAME, $CI_BUILD_REF_NAME $CI_BUILD_STAGE" # debug
  - export GRADLE_USER_HOME=`pwd`/.gradle
  script:
    - echo `$GRADLE_USER_HOME`
    - ./gradlew check -x test
  cache:
    key: gradle-cache-key
    paths:
      - .gradle/wrapper
      - .gradle/caches
    untracked: true
    policy: pull-push
#   artifacts:
#     paths:
#       - build/libs/*.jar
#     expire_in: 30 mins 4 sec
#   only:
#     - master
```

### VARIABLES

In the GitLab CI/CD there are many types of variables avilable.

-`projetct variables` - This types of variables use whole project. All the types of security information keys, usernames, tokens are stored as project variable in `settings -> CI/CD -> variables` section. If your branch is NOT protected, then your variable in GitLab should also be NOT protected. Otherwise (protected variable on non-protected branch) it will not work and you'll get docker login and ssh fail in the relevat stages.

-`pre-defined default variables` - This variables are pre-defined varibles by the GitLab. We can use thoese variables in the script without instroduction. Then, Gitlab identify it automatically.

- Some variables are defined top level of the script. Those varibles valid throughtout the script. All stages are used values of this variables.

- Some variables are defined within pnly the stages. Those variables are valid in the stage only. Other stages can't identify their value.

### BUILD STAGE

In this stage also, I have used openjdk:11 image to build container and run using the docker excutor. Also, I have used `gradle-cache-key` cache as a cache for this stage.Then it can use test stage downloaded files to this stage. From that cache option, we can minimize the time period which spent to sucess the job completion. In this build stage I build two images. I used GitLab CI/CD default variables as image name and tag name. Using this latest image I expect to deploy latest image on the test server using docker-compose file.


-`CI_REGISTRY_IMAGE` - This is a `default variable`. The address of the project’s Container Image Registry. Only available if the Container Registry is enabled for the project. You can see your image registry name `Package and Registries --> Container Registries --> CLI Commands` section.

-`CI_COMMIT_SHORT_SHA` - This is also a `default variable`.The first eight characters of commit revision the project is built for. 

Following script section is related to build stage in gitlab CI/CD pipeline.

```bash
build:
  stage: build
  image: openjdk:11
  only:
    - development
  script:
    - ./gradlew bootBuildImage --imageName="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - ./gradlew bootBuildImage --imageName="$CI_REGISTRY_IMAGE:latest"
  cache:
    key: gradle-cache-key
    paths:
      - .gradle/wrapper
      - .gradle/caches
    untracked: true
    policy: pull-push
```


## PUSH STAGE

In this stage, I have used docker:20.10.16 image to build docker image inside the docker excutor. As well as I have allocated docker service inside this container and to communicate purpose using TLS, among containers. I have added docker TLS certificate directory in variables section to store TLS certficates which use containers for communication each other. I paln to publish that build image to Gitlab Image Registry. For that, I got docker logging in the before script section.`CI_REG_USERNAME` and `CI_REG_TOKEN` provided as project variables in the `settings -> CI/CD -> variable` section. Before Script, Script, After Script sections are used to keep our script very clear. Usually, dependisies are written in the before and after script sections.

-`CI_REG_ACCESS_TOKEN_NAME` - This is project access token name. You can generate from this `settings --> Access Tokens` and generate an access token with Token name, Expiration date, Role, Scope. You can choose scope as both trad regostry and write registry. Copy the access-token key carefully, we need this belosection. You can configure this project access token name as a project variable in `settings --> CI/CD --> variables` section. Set type as `variables` and enable the `protect` and `mask` variable options.

-`CI_REG_ACCESS_TOKEN` - Token value which generate from upper part. Copy the access-token key carefully and configure it in;
`settings --> CI/CD --> variables`
section. Set type as `variables` and enable the `protect` and `mask` variable options also.

Following script section is related to push stage in gitlab CI/CD pipeline.

```bash
push:
  stage: push
  image: docker:20.10.16
  only:
    - development
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login sourcecontrol.hsenidmobile.com:5050 -u "$CI_REG_ACCESS_TOKEN_NAME" -p "$CI_REG_ACCESS_TOKEN"
  script:
    - apk update
    - apk add --no-cache tzdata && cp -r -f /usr/share/zoneinfo/Asia/Hong_Kong /etc/localtime
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker push "$CI_REGISTRY_IMAGE:latest"

```


## DEPLOY STAGE

In last, The deploy stage I expect to deploy this image on the server. Also this stage, I have used docker:20.10.16 image. My plan is ssh to deploy server using ssh key file and I have changed the permission of that key file as a security option. I have mentioned my whole senario as single ssh session in the script section. Also, I got a logging from deploy server to my gitlab image registry then, the deploy server can authenticate with gitlab image registry. Initially, we can do `git clone` option manually. Then we can use `git pull` option. In the docker compose file image tag sholud be `latest`. Then docker-compose helps to deploy latest image on the deploy server.   

- `DEV_SERVER_USER` - username which you expect to logging in to the server deploy the image

- `DEV_SERVER_IP` - deploy server ip

Above two variables are not default variables by Gitlab. Those are defined variables by myself for this project. You should onfigure it in `settings --> CI/CD --> variables` section. Set type as `variables` and enable the `protect` and `mask` variable options.

- `DEV_SERVER_ID_RSA` - private key which use authenticate with testing server. This is not a default variables. It is defined variables by myself for this project. configure it in `settings --> CI/CD --> variables` section. Set type as `file` and enable the `protect` variable option only.


You should manually clone the git repository before you run the deploy stage using ` clone with ssh` option. Otherwise, git pull command generate an error reguarding the ssh permissions. **Don't** clone with `Clone with HTTPS` option. First, You should generate a key pair in the deploy server and want to add an entry in the `~/.ssh/config` file in deploy server. Also, should paste public key in the gitlab instance.

To generate a ssh key pair and save the key in default `~/.ssh` directory.,
```bash
ssh-keygen -t rsa -b 2048
```

Then, need to paste public key in the gitlab repository `Profile --> Preferences -> SSH Keys` section with expiration date and a title. Then, edit the config file which is in the ~/.ssh/config path in deply server as below;
```bash
Host <host_url>
  HostName <host_name>
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/<private_key_file_name>
```

Create a directory to clone the gitlab instance in the server. Then, go inside to that directory and clone the Gitlab.

```bash
mkdir <directory_name>
cd <directory_name>
git clone <clone_with_ssh_url_which_copy_from_gitlab_instance>
```

And also, you should set up a deploy key in gitlab instance to deploy this built image to deploy server. first you should generate an another key pair and should paste public key in gitlab instance.

To generate a ssh key pair,
```bash
ssh-keygen -t rsa -b 2048
```

Save the key in default `~/.ssh` directory. Then, need to paste public key in the gitlab repository `Settings -> Repository -> Deploy Keys` section with a title. No need to enable grant write permissions to this key because we are not going to push this repository from server side. Then, log into testing server and open `~/.ssh` file.

```bash
vim ~/.ssh/known_hosts
```
Then you need to paste the public key in `known_hosts` file which is in `~/.ssh` directory. Open the `~/.ssh` file and paste the public key.

Make sure to save docker-compose file with correct image name which publish with latest tag. You can see your image registry name `Package and Registries --> Container Registries --> CLI Commands` section.

Following script section is related to deploy stage in gitlab CI/CD pipeline.

```bash
deploy:
  stage: deploy
  image: docker:20.10.16
  only:
    - development
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - chmod 400 $DEV_SERVER_ID_RSA
  script:
    - ssh -o StrictHostKeyChecking=no -i $DEV_SERVER_ID_RSA $DEV_SERVER_USER@$DEV_SERVER_IP "
        docker login sourcecontrol.hsenidmobile.com:5050 -u "$CI_REG_ACCESS_TOKEN_NAME" -p "$CI_REG_ACCESS_TOKEN" &&
        cd <created_directory_name> && 
        cd <directory_name_which_comes_from_clone> && 
        git pull && 
        cd docker &&
        docker-compose stop && 
        docker-compose rm -f && 
        docker-compose -f docker-compose.yml up -d"
```


# CONFIGURE THE FLOW OF STAGES

To run above stages step by step we need to mention stages respectevily.

```bash
stages:
  - test
  - build
  - push
  - deploy
```

```mermaid
graph TD;
  test stage-->built stage;
  build stage-->push stage;
  push stage-->deploy stage;
```

## FINAL FULL CODE


```bash
stages:
  - test
  - build
  - push
  - deploy


test:
  stage: test
  image: openjdk:11
  only:
    - development
  before_script:
  - echo `pwd` # debug
#  - echo "$CI_BUILD_NAME, $CI_BUILD_REF_NAME $CI_BUILD_STAGE" # debug
  - export GRADLE_USER_HOME=`pwd`/.gradle
  script:
    - echo `$GRADLE_USER_HOME`
    - ./gradlew check -x test
  cache:
    key: gradle-cache-key-1
    paths:
      - .gradle/wrapper
      - .gradle/caches
    untracked: true
    policy: pull-push
#   artifacts:
#     paths:
#       - build/libs/*.jar
#     expire_in: 30 mins 4 sec
#   only:
#     - master


build:
  stage: build
  image: openjdk:11
  only:
    - development
  script:
    - ./gradlew bootBuildImage --imageName="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - ./gradlew bootBuildImage --imageName="$CI_REGISTRY_IMAGE:latest"
  cache:
    key: gradle-cache-key-1
    paths:
      - .gradle/wrapper
      - .gradle/caches
    untracked: true
    policy: pull-push


push:
  stage: push
  image: docker:20.10.16
  only:
    - development
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login sourcecontrol.hsenidmobile.com:5050 -u "$CI_REG_ACCESS_TOKEN_NAME" -p "$CI_REG_ACCESS_TOKEN"
  script:
    - apk update
    - apk add --no-cache tzdata && cp -r -f /usr/share/zoneinfo/Asia/Hong_Kong /etc/localtime
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker push "$CI_REGISTRY_IMAGE:latest"


deploy:
  stage: deploy
  image: docker:20.10.16
  only:
    - development
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - chmod 400 $DEV_SERVER_ID_RSA
  script:
    - ssh -o StrictHostKeyChecking=no -i $DEV_SERVER_ID_RSA $DEV_SERVER_USER@$DEV_SERVER_IP "
        docker login sourcecontrol.hsenidmobile.com:5050 -u "$CI_REG_ACCESS_TOKEN_NAME" -p "$CI_REG_ACCESS_TOKEN" &&
        cd <created_directory_name> && 
        cd <directory_name_which_comes_from_clone> && 
        git pull && 
        cd docker &&
        docker-compose stop && 
        docker-compose rm -f && 
        docker-compose -f docker-compose.yml up -d"
```